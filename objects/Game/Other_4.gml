// Enable views and set up graphics

view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();



if (room == room_Game_1) {
		
	// Define pathfinding grid
		
	grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
	// Add collision tiles to grid			
	for (var _col = 0; _col < room_width/GRID_RESOLUTION; _col++) {
		for (var _row = 0; _row < room_height/GRID_RESOLUTION; _row++) {				
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) != 0) {
				mp_grid_add_cell(grid, _col, _row);
			}
		}
	}
	
	
	// Initialize particle system and emitters
		
	particle_system = part_system_create_layer("lyr_Particles", true);
	particle_emitter = part_emitter_create(particle_system);
		
	// Initialize player
	if (instance_exists(obj_Player)) {
		with (obj_Player) {
			x = room_width/2;
			y = irandom_range(room_height-200, room_height-100);
			event_perform(ev_other, ev_user0);
		}
	}
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Play music	
	audio_stop_all();
	if (option_value[? "Music"]) {
		music_sound_id = audio_play_sound(snd_Music, 1, true);
	}

}
else if (room == room_UI_Title) {
	audio_stop_all();
	if (option_value[? "Music"]) {
		music_sound_id = audio_play_sound(snd_Intro, 1, true);
	}
	alarm[3] = 60*3;	
}