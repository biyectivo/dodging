//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();

//*****************************************************************************
// Set camera target
//*****************************************************************************

camera_target = obj_Player;
camera_shake = false;
camera_smoothness = 0.15;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "ascend";
control_indices[1] = "descend";
control_indices[2] = "accelerate";
control_indices[3] = "decelerate";

controls[? "ascend"] = vk_left;
controls[? "descend"] = vk_right;
controls[? "accelerate"] = vk_up;
controls[? "decelerate"] = vk_down;


control_names[? "ascend"] = "Steer counterclockwise";
control_names[? "descend"] = "Steer clockwise";
control_names[? "accelerate"] = "Accelerate";
control_names[? "decelerate"] = "Decelerate";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021[c_yellow]Programming: [c_white]biyectivo";
credits[1] = "[fa_middle][fa_center][c_white][c_white](Jose Alberto Bonilla Vera)";

game_title = "One Broom";
scoreboard_game_id = "One Broom";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter_fire = noone;

pause_screenshot = noone;

//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;

sprite_palette = palette_sprite;
palette_surface = noone;

color_being_modified = -1;	

fnc_InitializeGameStartVariables();

acceleration_sensitivity = [0.1,0.25,0.4];
deceleration_sensitivity = [0.1,0.25,0.4];
steering_sensitivity = [2,4,6];
	
acceleration_sensitivity_index = 1;
deceleration_sensitivity_index = 2;
steering_sensitivity_index = 1;

palette_surface = noone;
colors = array_create(sprite_get_height(palette_sprite),-1);