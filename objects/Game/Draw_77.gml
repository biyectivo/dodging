/// @description Draw Game

gpu_set_blendenable(false);
offset_x_GUI = 0;
if (SELECTED_SCALING == SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION) {
	
	if (window_get_fullscreen()) {
		draw_surface_ext(application_surface, 0, 0, DISPLAY_WIDTH/adjusted_camera_width, DISPLAY_HEIGHT/adjusted_camera_height, 0, c_white, 1);
		display_set_gui_size(DISPLAY_WIDTH, DISPLAY_HEIGHT);	
	}
	else {
		draw_surface_ext(application_surface, 0, 0, 1, 1, 0, c_white, 1);
		display_set_gui_size(adjusted_window_width, adjusted_window_height);	
	}
}
else if (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION) {
	if (window_get_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_window_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_window_height;
		var _scalew = adjusted_camera_width * _factorw;
		var _scaleh = adjusted_camera_height * _factorh;
		var _offsetx = (DISPLAY_WIDTH - _scalew)/2;
		var _offsety = (DISPLAY_HEIGHT - _scaleh)/2;
		offset_x_GUI = _offsetx;
		draw_surface_ext(application_surface, _offsetx, _offsety, _factorw, _factorh, 0, c_white, 1);
		display_set_gui_size(DISPLAY_WIDTH, DISPLAY_HEIGHT);
	}
	else {		
		var _offsetx = (adjusted_window_width - adjusted_camera_width)/2;
		var _offsety = (adjusted_window_height - adjusted_camera_height)/2;
		offset_x_GUI = _offsetx;
		draw_surface_ext(application_surface, _offsetx, _offsety, 1, 1, 0, c_white, 1);
		display_set_gui_size(adjusted_window_width, adjusted_window_height);	
	}
}
else { // RESOLUTION_SCALED_TO_WINDOW
	if (window_get_fullscreen()) {
		var _factorw = min(DISPLAY_WIDTH/adjusted_camera_width,DISPLAY_HEIGHT/adjusted_camera_height);
		var _factorh = _factorw;
		
		var _scalew = adjusted_camera_width*_factorw;
		var _scaleh = adjusted_camera_height*_factorh;
		var _offsetx = (DISPLAY_WIDTH - _scalew)/2;
		var _offsety = (DISPLAY_HEIGHT - _scaleh)/2;
		
		draw_surface_ext(application_surface, _offsetx, _offsety, _factorw, _factorh, 0, c_white, 1);		
		display_set_gui_size(DISPLAY_WIDTH, DISPLAY_HEIGHT);
		display_set_gui_maximize(1, 1, 0, 0);		
		
	}
	else {
		var _factorw = min(adjusted_window_width/adjusted_camera_width,adjusted_window_height/adjusted_camera_height);
		var _factorh = _factorw;
		var _scalew = adjusted_camera_width*_factorw;
		var _scaleh = adjusted_camera_height*_factorh;
		var _offsetx = (adjusted_window_width - _scalew)/2;
		var _offsety = (adjusted_window_height - _scaleh)/2;
		
		draw_surface_ext(application_surface, _offsetx, _offsety, _factorw, _factorh, 0, c_white, 1);
		display_set_gui_size(adjusted_window_width, adjusted_window_height);	
	}
}

gpu_set_blendenable(true);
