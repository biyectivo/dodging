if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (room == room_Game_1 && Game.lost && Game.paused && ((primary_gamepad != -1 && (gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) && !keyboard_check(vk_lalt)) {
	room_restart();
}

// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		
	}
}




show_debug_overlay(Game.debug);