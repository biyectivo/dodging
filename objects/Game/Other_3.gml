/// @description 
surface_free(palette_surface);

part_emitter_destroy_all(particle_system);
part_emitter_destroy_all(particle_system2);

if (part_system_exists(particle_system)) {
	part_system_destroy(particle_system);
}
if (part_system_exists(particle_system2)) {
	part_system_destroy(particle_system2);
}
