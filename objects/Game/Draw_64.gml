//draw_surface_ext(palette_surface, 0, 0, 10, 10, 0, c_white, 1);
fnc_BackupDrawParams();

switch (room) {
	case room_UI_Title:
		fnc_DrawMenu();
		break;
	case room_UI_Options:
		fnc_DrawOptions();
		break;
	case room_UI_Options_Controls:
		fnc_DrawOptionsControls();
		break;
	case room_UI_Credits:
		fnc_DrawCredits();		
		break;
	case room_UI_HowToPlay:
		fnc_DrawHowToPlay();
		break;
	default:		
		if (Game.paused) {
			if (Game.lost) {
				fnc_DrawYouLost();
			}
			else {				
				fnc_DrawPauseMenu();
			}
		}
		else {			
			fnc_DrawHUD();
		}
		break;
}

// Draw debug overlay on top of everything else
if (debug) {
	fnc_DrawDebug();
}

fnc_RestoreDrawParams();

