/// @description 

if (!surface_exists(palette_surface)) {	
	palette_surface = surface_create(sprite_get_width(palette_sprite), sprite_get_height(palette_sprite));
	surface_set_target(Game.palette_surface);
	draw_sprite(palette_sprite, 0, 0, 0);
	
	var _n = sprite_get_height(palette_sprite);
	for (var _i=0; _i<_n; _i++) {
		if (colors[_i] != -1) {
			draw_set_color(colors[_i]);
			draw_point(1, _i);	
		}
	}
	
	surface_reset_target();
}


if (!paused) {

	if (room == room_Game_1) {			
		//draw_rectangle_color(0, 0, room_width, room_height-32, c_red, c_purple, $660000, $aa0000, false);	
		draw_rectangle_color(0, 0, room_width, room_height-32, gradients[gradient_index][0], gradients[gradient_index][1], gradients[gradient_index][2], gradients[gradient_index][3], false);	
		draw_rectangle_color(0, room_height-32, room_width, room_height, c_black, c_black, c_black, c_black, false);
	
		if (!part_system_exists(particle_system2)) {
			particle_system2 = part_system_create_layer("lyr_Stars", false);
		}
		if (!part_emitter_exists(particle_system2, particle_emitter2)) {
			particle_emitter2 = part_emitter_create(particle_system2);	
		}
		
	
		// Draw silver stars everywhere	
		particle_type_star =	part_type_create();		
		part_type_scale(particle_type_star, 1, 1);
		part_type_size(particle_type_star, 0.1, 0.2, 0, 0);	
		part_type_life(particle_type_star, 60*4, 60*8);			
		part_type_shape(particle_type_star, pt_shape_star);
		part_type_color1(particle_type_star, fnc_TypeFormatted_Color("c_silver"));
		part_type_alpha3(particle_type_star, 0, 0.4, 0);
		part_type_speed(particle_type_star, 0, 0, 0, 0);
		part_type_direction(particle_type_star, 0, 0, 0, 0);
		part_type_orientation(particle_type_star, 0, 0, 0, 0, false);	
		part_type_blend(particle_type_star, false);	
		part_emitter_region(Game.particle_system2, Game.particle_emitter2, 0, room_width, 0, room_height, ps_shape_rectangle, ps_distr_gaussian);
		part_emitter_stream(Game.particle_system2, Game.particle_emitter2,  particle_type_star, 5);
	
	}
}