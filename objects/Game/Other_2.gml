

// Mouse cursor
window_set_cursor(cr_none);
cursor_sprite = spr_Cursor;

// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
application_surface_draw_enable(false);

// Retro palette swapper
pal_swap_init_system(shd_pal_swapper, shd_pal_html_sprite, shd_pal_html_surface);

// Go to next room
//room_goto(room_UI_Title);
room_goto(room_UI_Title);
