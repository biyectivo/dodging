/*
show_debug_message("Async load");
show_debug_message(async_load[? "id"]);
show_debug_message(async_load[? "status"]);
show_debug_message(async_load[? "result"]);
show_debug_message(async_load[? "url"]);
show_debug_message(async_load[? "http_status"]);
*/

//show_debug_message("Called async event from "+string(async_load[? "id"])+", insert is "+string(http_get_id_update)+" and scoreboard is "+string(http_get_id_query));

if (async_load[? "id"] == http_get_id_query && async_load[? "status"] == 0) {
	show_debug_message("Scoreboard query "+string(async_load[? "id"])+" returned ok");
	http_return_status_query = async_load[? "http_status"];
	http_return_result_query = async_load[? "result"];
	//scoreboard = json_parse(async_load[? "result"]);
	scoreboard_html5 = json_decode(async_load[? "result"]);
}
else if (async_load[? "id"] == http_get_id_update && async_load[? "status"] == 0) {
	show_debug_message("Score inserted successfully");
	http_return_status_update = async_load[? "http_status"];
	http_return_result_update = async_load[? "result"];
	show_debug_message(string(http_return_status_update));
}

/*
if (async_load[? "status"] == 0 && http_call == "query_scoreboard") {
	show_debug_message("Result");
	show_debug_message(async_load[? "result"]);
	show_debug_message("Result ends here");
	scoreboard = json_parse(async_load[? "result"]);	
	show_debug_message("after");
}*/