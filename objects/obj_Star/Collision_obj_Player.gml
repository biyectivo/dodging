/// @description 
if (obj_Player.state != "Die") {
	Game.total_score = Game.total_score + STAR_VALUE;
	Game.total_stars++;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(choose(snd_Star1, snd_Star2, snd_Star3), 3, false);
	}
	instance_destroy();
}