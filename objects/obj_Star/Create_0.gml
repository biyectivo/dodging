/// @description 
alarm[0] = 60;
image_angle = irandom_range(0,360);
rotation_speed = irandom_range(1, 10);
rotation_sign = choose(1, -1);
fall_speed = irandom_range(2, 6);