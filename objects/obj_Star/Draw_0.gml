/// @description 
if (!Game.paused) {
	var _size_modifier = max(0.2, sin(PI*alarm[0]/60));
	image_angle = image_angle + rotation_sign * rotation_speed;
	//show_debug_message(_size_modifier);
	draw_sprite_ext(spr_Star, 0, x, y, _size_modifier, _size_modifier, image_angle, c_white, 1);
}