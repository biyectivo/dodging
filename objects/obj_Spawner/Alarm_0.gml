/// @description Create star
if (instance_exists(obj_Player) && obj_Player.state != "Die") {
	var _id = instance_create_layer(irandom_range(32, room_width-32), 32, layer_get_id("lyr_Instances"), obj_Star);
	alarm[0] = irandom_range(create_min, create_max);
}