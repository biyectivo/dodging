/// @description Create spell
if (instance_exists(obj_Player) && obj_Player.state != "Die") {
	Game.level++;

	var _x = irandom_range(-64, room_width+64);
	var _y = -64;


	var _id = instance_create_layer(_x, _y, layer_get_id("lyr_Instances"), obj_Spell);
	alarm[1] = max(30, 70 - (Game.level-2));
}