/// @description 
if (!Game.paused) {
	if (instance_exists(obj_Player) && obj_Player.state != "Die" && instance_number(obj_Ring_Below) == 0 && instance_number(obj_Ring_Above) == 0) {
		var _x = irandom_range(64, room_width-64);
		var _y = irandom_range(64, room_height-200);
		var _id1 = instance_create_layer(_x, _y, layer_get_id("lyr_Rings_Below"), obj_Ring_Below);
		var _id2 = instance_create_layer(_x, _y, layer_get_id("lyr_Rings_Above"), obj_Ring_Above);
	}
}