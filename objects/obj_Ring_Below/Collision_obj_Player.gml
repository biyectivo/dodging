/// @description 
if (obj_Player.bbox_top > bbox_top && obj_Player.bbox_bottom < bbox_bottom && obj_Player.state != "Die") {
	if (Game.collided_ring == RING.NONE) {
		Game.alarm[2] = RING_TIMEOUT;
		Game.collided_ring = RING.BELOW;
		Game.collided_ring_id = id;
	}
	else if (Game.collided_ring == RING.ABOVE && Game.alarm[2] != -1) { // collision with both within timeout	
		Game.collided_ring = RING.NONE;
		Game.total_hoops++;
		Game.total_score = Game.total_score + RING_MULTIPLIER*Game.total_hoops;		
		with (Game.collided_ring_id) {
			instance_destroy();
		}
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Ring, 3, false);
		}
		instance_destroy();
	}
}