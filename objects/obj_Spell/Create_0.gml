/// @description 

if (Game.level < 10) {
	var _probs = [0.8, 0.1, 0.07, 0.03];
}
else if (Game.level < 30) {
	var _probs = [0.6, 0.2, 0.12, 0.08];
}
else if (Game.level < 60) {
	var _probs = [0.4, 0.3, 0.2, 0.1];
}
else if (Game.level < 90) {
	var _probs = [0.1, 0.2, 0.3, 0.4];
}
else {
	var _probs = [0, 0, 0, 1];
}

image_index = fnc_ChooseProb([], _probs);
var _angle_with_player = point_direction(x, y, obj_Player.x, obj_Player.y);
var _angle_variance = max(5-floor(Game.level/10), 0);
image_angle = _angle_with_player + irandom_range(-_angle_variance, _angle_variance); 
spell_speed = (image_index+1) * 2 + 1;
//show_debug_message("Created spell of type "+string(image_index)+" with angle "+string(image_angle)+" (variance="+string(_angle_variance)+")");

if (Game.option_value[? "Sounds"]) {
	audio_play_sound(snd_Spell, 2, false);
}
