/// @description 
if (!Game.paused) {
	var _x1 = x-half_width;
	var _x2 = x+half_width
	var _y1 = room_height-height;
	var _y2 = room_height;
	
	draw_rectangle_color(_x1, _y1, _x2, _y2, c_black, c_black, c_black, c_black, false);
	
	
	for (var _row=0; _row<rows; _row++) {
		for (var _col=0; _col<cols; _col++) {
			if (window[_row][_col]) {
				var _color = c_yellow;
			}
			else {
				var _color = $333333;	
			}
			draw_rectangle_color(_x1+min_border+additional_border_col+floor_separation/2+_col*(window_side+floor_separation),
								_y1+min_border+additional_border_row+floor_separation/2+_row*(window_side+floor_separation),
								_x1+min_border+additional_border_col+floor_separation/2+_col*(window_side+floor_separation)+window_side,
								_y1+min_border+additional_border_row+floor_separation/2+_row*(window_side+floor_separation)+window_side,							
								_color, _color, _color, _color, false);
		}
	}
}