/// @description 
half_width = irandom_range(40,100);
height = irandom_range(100, 600);


window_side = 20;
floor_separation = irandom_range(5,10);
min_border = 10;
	
rows = floor((height-2*min_border)/(window_side+floor_separation));
additional_border_row = (height-2*min_border-rows*(window_side+floor_separation))/2;
	
cols = floor((2*half_width-2*min_border)/(window_side+floor_separation));
additional_border_col = (2*half_width-2*min_border-cols*(window_side+floor_separation))/2;
window = [];

window_prob = random_range(0.1,0.3);

for (var _j=0; _j<rows; _j++) {
	for (var _i=0; _i<cols; _i++) {
		window[_j][_i] = (random(1) < window_prob);		
	}
}