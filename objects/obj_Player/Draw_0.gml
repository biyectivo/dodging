if (!Game.paused && initialized) {
	
	//pal_swap_set(Game.sprite_palette, 1, false);
	pal_swap_set(Game.palette_surface, 1, true);
	
	if (script_exists(asset_get_index("fnc_PlayerAnimateFSM_"+state))) {
		script_execute(asset_get_index("fnc_PlayerAnimateFSM_"+state));	
	}	
	
	pal_swap_reset();
	
	//draw_line_color(bbox_left, bbox_top, bbox_right, bbox_top, c_green, c_green);
	//draw_line_color(bbox_left, bbox_bottom, bbox_right, bbox_bottom, c_green, c_green);
	
	// Draw arrow
	if (instance_exists(obj_Ring_Below) && distance_to_object(obj_Ring_Below) > 250) {
		
		var _angle = point_direction(x, y, obj_Ring_Below.x, obj_Ring_Above.y);
		
		var _xarrow_ori = x+24;
		var _yarrow_ori = y;
		//var _xarrow_ori = x + 32*cos(degtorad(_angle));
		//var _yarrow_ori = y - 32*sin(degtorad(_angle));
		
		var _distance = point_distance(x, y, _xarrow_ori, _yarrow_ori);
		var _angle_difference = angle_difference(image_angle, point_direction(x, y, _xarrow_ori, _yarrow_ori));
	
		var _xarrow = x + lengthdir_x(_distance, _angle_difference);
		var _yarrow = y + lengthdir_y(_distance, _angle_difference);
	
		
		draw_sprite_ext(spr_Arrow, 1, _xarrow, _yarrow, 0.7, 0.7, _angle, c_white, 0.5);
	}
}
