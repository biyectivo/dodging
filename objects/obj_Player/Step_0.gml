if (!Game.paused && initialized) {
	previous_state = state;

	// Execute current state
	
	if (script_exists(asset_get_index("fnc_PlayerFSM_"+state))) {
		script_execute(asset_get_index("fnc_PlayerFSM_"+state));	
	}
}
