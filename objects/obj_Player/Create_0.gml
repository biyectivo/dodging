/// @description 
fly_speed = 1;
min_fly_speed = 0.5;
max_fly_speed = 10;

fly_acceleration = Game.acceleration_sensitivity[Game.acceleration_sensitivity_index];
fly_deceleration = Game.deceleration_sensitivity[Game.deceleration_sensitivity_index];
rotation_speed = Game.steering_sensitivity[Game.steering_sensitivity_index];

state = "Fly";
previous_state = state;

hp = 1;

already_played_crash_sound = false;