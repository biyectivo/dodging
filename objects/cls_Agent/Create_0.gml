/// @description Create agent

event_inherited(); // Initialize false

#region Overridable data

	// General
	name = "Agent";
	controllable = true;
	animation_dirs = 4;

	// Stats
	walk_hspeed = 2;
	walk_vspeed = 2;
	max_hp = 1;
	hp = max_hp;

	// Animation	
	animation_startindices = ds_map_create();
	animation_lengths = ds_map_create();
	animation_spacings = ds_map_create();
	animation_speeds = ds_map_create();

	// Beginning sprite frame index for each state
	animation_startindices[? "Idle"] = 0;
	animation_startindices[? "Move"] = 0;
	animation_startindices[? "Attack"] = 0;
	animation_startindices[? "Die"] = 0;

	// Length in frames of animation for each state
	animation_lengths[? "Idle"] = 1;
	animation_lengths[? "Move"] = 1;
	animation_lengths[? "Attack"] = 1;
	animation_lengths[? "Die"] = 1;

	// Spacing between frames of animation for each state (default = 1)
	animation_spacings[? "Idle"] = 1;
	animation_spacings[? "Move"] = 1;
	animation_spacings[? "Attack"] = 1;
	animation_spacings[? "Die"] = 1;

	// Animation speed for each frame - lower number means higher speed
	animation_speeds[? "Idle"] = 30;
	animation_speeds[? "Move"] = 30;
	animation_speeds[? "Attack"] = 30;
	animation_speeds[? "Die"] = 30;

#endregion

#region Other, normally not overridable data

	base_xscale = 1;

#endregion