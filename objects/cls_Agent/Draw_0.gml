/// @description Draw agent
if (!Game.paused && initialized) {	
	fnc_BackupDrawParams();
	
	draw_self();
	
	if (Game.debug) {
		draw_set_alpha(0.7);
		draw_set_color(c_red);
		draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_text(x, y-16, "Index: "+string(image_index));
	}
	fnc_RestoreDrawParams();
}