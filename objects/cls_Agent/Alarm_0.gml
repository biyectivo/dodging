/// @description Animator
if (!Game.paused && initialized) {	
	script_execute(asset_get_index("fnc_"+name+"FSM_Animate"), reset_animation);
	alarm[0] = animation_speeds[? animation_name];
}