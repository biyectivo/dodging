
/// @function fnc_SetGraphics
/// @description Update graphics

function fnc_SetGraphics() {
	

	if (argument_count>1) {
		var _RESOLUTION_W = argument[0];
		var _RESOLUTION_H = argument[1];
	}
	else {
		var _RESOLUTION_W = BASE_RESOLUTION_W;
		var _RESOLUTION_H = BASE_RESOLUTION_H;
	}
	
	if (!MOBILE_DEVICE) {
		var _used_aspect_ratio = ASPECT_RATIO_REAL;	
	}
	else {
		var _used_aspect_ratio = ASPECT_RATIO_FORCED;
	}
	

	if (SELECTED_SCALING == SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION) { // Adjust desired Game resolution (camera) to match display size/aspect ratio and then set window equal to game resolution		
		if (_used_aspect_ratio<1) { // Portrait aspect ratio
			adjusted_camera_width = min(_RESOLUTION_W, DISPLAY_WIDTH);
			adjusted_camera_height = floor(adjusted_camera_width / _used_aspect_ratio);
		}
		else { // Landscape aspect ratio
			adjusted_camera_height = min(_RESOLUTION_H, DISPLAY_HEIGHT);
			adjusted_camera_width = floor(adjusted_camera_height * _used_aspect_ratio);
		}
		
		if (window_get_fullscreen()) {
			adjusted_window_width = DISPLAY_WIDTH;
			adjusted_window_height = DISPLAY_HEIGHT;
		}
		else {			
			adjusted_window_width = adjusted_camera_width;
			adjusted_window_height = adjusted_camera_height;
		}
	}
	else if (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION) { // Leave Game resolution (camera) fixed (as long as <= window) and adjust desired window resolution to match display size/aspect ratio
		
		if (_used_aspect_ratio < 1) { // Portrait aspect ratio
			adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
			adjusted_window_height = floor(adjusted_window_width / _used_aspect_ratio);
		}
		else { // Landscape aspect ratio
			adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
			adjusted_window_width = floor(adjusted_window_height * _used_aspect_ratio);
		}
		
		adjusted_camera_width = min(_RESOLUTION_W, BASE_WINDOW_SIZE_W);
		adjusted_camera_height = min(_RESOLUTION_H, BASE_WINDOW_SIZE_H);		
		
	}
	else { // Resolution scaled to window size
		if (_used_aspect_ratio < 1) { // Portrait aspect ratio
			adjusted_camera_width = min(_RESOLUTION_W, DISPLAY_WIDTH);			
			adjusted_camera_height = floor(adjusted_camera_width / _used_aspect_ratio);			
		}
		else { // Landscape aspect ratio 
			adjusted_camera_height = min(_RESOLUTION_H, DISPLAY_HEIGHT);
			adjusted_camera_width = floor(adjusted_camera_height * _used_aspect_ratio);
		}
				
		if (_used_aspect_ratio < 1) { // Portrait aspect ratio			
			adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
			adjusted_window_height = floor(adjusted_window_width / _used_aspect_ratio);			
		}
		else { // Landscape aspect ratio
			if (os_browser == browser_not_a_browser || os_browser != browser_not_a_browser && !option_value[? "Fullscreen"]) {
				adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
				adjusted_window_width = floor(adjusted_window_height * _used_aspect_ratio);
			}
			else  {
				adjusted_window_width = browser_width;
				adjusted_window_height = browser_height;
			}
		}
		
	}
	
	// Redefine camera size
	camera_set_view_pos(VIEW, 0, 0);
	camera_set_view_size(VIEW, adjusted_camera_width, adjusted_camera_height);
		
	// Set window size	
	window_set_size(adjusted_window_width, adjusted_window_height);
	
	// Browser maximize hack
	
	if (os_browser != browser_not_a_browser && option_value[? "Fullscreen"]) {
		window_set_size(browser_height*_used_aspect_ratio, browser_height);
	}
	
	// Resize the application surface to the desired game resolution width/height
	surface_resize(application_surface, adjusted_camera_width, adjusted_camera_height);
	
	
	
	
	// Center the window (on the next few step)
	if (CENTER_SCREEN) {
		alarm[0] = 2;
	}
	
}
