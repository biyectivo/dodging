function fnc_PlayerFSM_Fly() {
	
	var _commit_x = x + fly_speed * cos(degtorad(image_angle));
	if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _commit_x, y) == 1) {
		obj_Player.hp = 0;
	}
	else if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _commit_x, y) != 3) {		
		x = _commit_x;
	}
	
	var _commit_y = y - fly_speed * sin(degtorad(image_angle));
	if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), x, _commit_y) == 1) {
		obj_Player.hp = 0;
	}
	else if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), x, _commit_y) != 3) {
		y = _commit_y;
	}
	
	var _up = keyboard_check(Game.controls[? "ascend"]);	
	var _down = keyboard_check(Game.controls[? "descend"]);
	var _decelerate = keyboard_check(Game.controls[? "decelerate"]);
	var _accelerate = keyboard_check(Game.controls[? "accelerate"]);
	
	if (_up) {
		image_angle = (image_angle + rotation_speed) % 360;
	}
	else if (_down) {
		image_angle = (image_angle - rotation_speed) % 360;
	}
	if (_accelerate) {
		fly_speed = min(fly_speed + fly_acceleration, max_fly_speed);
	}
	else if (_decelerate) {
		fly_speed = max(fly_speed - fly_deceleration, min_fly_speed);
	}
	
	fnc_PlayerFSM_Transition();
}
function fnc_PlayerFSM_Jump() {
	
	fnc_PlayerFSM_Transition();
}
function fnc_PlayerFSM_Transition() {
	var _jump = false;
	
	if (hp <= 0) {
		state = "Die";
	}
	else if (_jump) {
		state = "Jump";
	}
	else {
		state = "Fly";
	}
}

function fnc_PlayerFSM_Die() {
	if (alarm[1] == -1) {
		alarm[1] = 180;
	}
	
	if (visible) {
		visible = false;
		particle_type_die_fragment =	part_type_create();		
		part_type_scale(particle_type_die_fragment, 1, 1);
		part_type_size(particle_type_die_fragment, 1,1, 0, 0);
		//part_type_life(particle_type_die_fragment, 60, 120);			
		part_type_life(particle_type_die_fragment, 60, 180);			
		part_type_shape(particle_type_die_fragment, pt_shape_pixel);
		part_type_color3(particle_type_die_fragment, c_white, fnc_TypeFormatted_Color("c_gold"), c_aqua);
		part_type_alpha2(particle_type_die_fragment, 1, 0);
		part_type_speed(particle_type_die_fragment, 0.1*fly_speed, 1*fly_speed, 0, 0);
		part_type_direction(particle_type_die_fragment, 0, 360, 0, 0);	
		part_type_blend(particle_type_die_fragment, true);
	
		// disable trail
		part_emitter_stream(Game.particle_system, Game.particle_emitter,  particle_type_trail, 0);
	
		part_emitter_region(Game.particle_system, Game.particle_emitter, x-32, x+32, y-32, y, ps_shape_ellipse, ps_distr_gaussian);
		part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_die_fragment, 5000);	
	}
	
	if (audio_is_playing(Game.music_sound_id)) {
		audio_stop_sound(Game.music_sound_id);		
	}
	
	if (!already_played_crash_sound) {
		already_played_crash_sound = true;
		audio_stop_all();
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Crash, 2, false);
		}
	}
}

function fnc_PlayerAnimateFSM_Fly() {
	draw_sprite_ext(spr_Player_Fly,0,x,y,1, 1, image_angle, c_white, 1);
	draw_sprite_ext(spr_Broom,0,x,y,1,1,image_angle,c_white,1);
	
	if (keyboard_check_pressed(vk_backspace)) {
		image_angle = 0;
	}
	
	// Particles
	
	particle_type_trail =	part_type_create();		
	part_type_scale(particle_type_trail, 1, 1);
	part_type_size(particle_type_trail, 0.1, 0.05, 0, 0);
	//part_type_life(particle_type_trail, 60, 120);			
	part_type_life(particle_type_trail, 60, 180);			
	part_type_shape(particle_type_trail, pt_shape_star);
	part_type_color1(particle_type_trail, choose(c_white, fnc_TypeFormatted_Color("c_gold"), c_aqua));
	part_type_alpha2(particle_type_trail, 0.5, 0.1);
	part_type_speed(particle_type_trail, 0.6*fly_speed, 1.1*fly_speed, 0, 0);
	part_type_direction(particle_type_trail, image_angle+180-3, image_angle+180+3, 0, 0);
	part_type_orientation(particle_type_trail, 0, 360, 0.5, 0, false);	
	part_type_blend(particle_type_trail, true);
	
	draw_set_color(c_black);
	
	var _xbroom_ori = x-16;
	var _ybroom_ori = y-8;
	var _distance = point_distance(x, y, _xbroom_ori, _ybroom_ori);
	var _angle_difference = angle_difference(image_angle, point_direction(x, y, _xbroom_ori, _ybroom_ori));
	
	var _xbroom = x + lengthdir_x(_distance, _angle_difference);
	var _ybroom = y + lengthdir_y(_distance, _angle_difference);
	
	part_emitter_region(Game.particle_system, Game.particle_emitter, _xbroom-2, _xbroom+2, _ybroom-2, _ybroom+2, ps_shape_line, ps_distr_gaussian);
	//part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_trail, 1);	
	part_emitter_stream(Game.particle_system, Game.particle_emitter,  particle_type_trail, round(4+3*fly_speed/5));
}
function fnc_PlayerAnimateFSM_Jump() {
	draw_sprite_ext(spr_Player_Jump,0,x,y,1, 1, image_angle, c_white, 1);
	
}
