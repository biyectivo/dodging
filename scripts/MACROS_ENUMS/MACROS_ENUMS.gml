// =============================================== Graphics =============================================== 
#macro VIEW view_camera[0]

// NOTE: Room size, does not play here. it can be as big or as little.

// 0 - Display size and aspect ratio
// Leave it calculated to get it automatically, or you can force it to a specific size to simulate other screens.
#macro DISPLAY_WIDTH display_get_width()
#macro DISPLAY_HEIGHT display_get_height()

#macro MOBILE_DEVICE ((os_type == os_android || os_type == os_ios) && os_browser == browser_not_a_browser)
#macro ASPECT_RATIO_REAL DISPLAY_WIDTH/DISPLAY_HEIGHT
#macro ASPECT_RATIO_FORCED real(540/960)

enum SCALING_TYPE {
	WINDOW_SAME_AS_RESOLUTION, // set resolution and let window adapt to that resolution
	WINDOW_INDEPENDENT_OF_RESOLUTION, // Set resolution AND set window size independently (e.g. bosom)
	RESOLUTION_SCALED_TO_WINDOW // "normal" mode - set resolution AND window size and let resolution scale to window size
}

#macro SELECTED_SCALING SCALING_TYPE.RESOLUTION_SCALED_TO_WINDOW

// 1 - Base resolution: this is the game resolution it's designed for (this will be the camera view size)
//		Notes: to achieve a perfect scaling for a 16:9 aspect ratio, one can take these values:
//		1x	1920x1080 
//		2x	960x540
//		3x	640x360
//		4x	480x270
//		5x	384x216
//		6x	320x180
//		Non perfect scaling: 1280x720
#macro BASE_RESOLUTION_W 760
#macro BASE_RESOLUTION_H 360

// 2 - Window resolution: OPTIONAL. Set this only if you want the WINDOW to be different size than the actual GAME RESOLUTION
// Applies to WINDOW_INDEPENDENT_OF_RESOLUTION (in this case there will be no scaling) and to RESOLUTION_SCALED_TO_WINDOW (in this case the resolution will scale)
// For RESOLUTION_SCALED_TO_WINDOW, for landscape aspect ratios, it will adjust the width automatically (so choose height wisely); for portrait aspect ratios, it will adjust the height automatically (so choose width wisely)
#macro BASE_WINDOW_SIZE_W 1520 //960
#macro BASE_WINDOW_SIZE_H 720 //540

#macro CENTER_SCREEN true

// =============================================== Technical =============================================== 

#macro TILE_SIZE 16
#macro GRID_RESOLUTION 16
#macro GAMEPAD_THRESHOLD 0.3

#macro ENABLE_SCOREBOARD true
#macro SCOREBOARD_SALT "b1yEctiv0"

enum TRANSITION {
	FADE_OUT,
	STRIPES_HORIZONTAL,
	STRIPES_VERTICAL,
	SQUARES
}

enum FACING {
	EAST,
	WEST,
	NORTH,
	SOUTH
}


#macro SCREENSHAKE_STRENGTH 10
#macro SCREENSHAKE_DURATION 30

// ICONS


#macro ICON_MUSIC_OFF		chr(base_convert("e440", 16, 10))
#macro ICON_MUSIC_ON		chr(base_convert("e405", 16, 10))
#macro ICON_SOUND_ON		chr(base_convert("e050", 16, 10))
#macro ICON_SOUND_OFF		chr(base_convert("e04f", 16, 10))
#macro ICON_RETRY			chr(base_convert("e042", 16, 10))
#macro ICON_PAUSE			chr(base_convert("e034", 16, 10))
#macro ICON_PALETTE			chr(base_convert("e40a", 16, 10))
#macro ICON_CONTROLS		chr(base_convert("ea28", 16, 10))
#macro ICON_SLIDERS			chr(base_convert("e429", 16, 10))
#macro ICON_HELP			chr(base_convert("e8fd", 16, 10))
#macro ICON_FULLSCREEN_OFF	chr(base_convert("e5d0", 16, 10))
#macro ICON_FULLSCREEN_ON	chr(base_convert("e5d1", 16, 10))


#macro PI 3.1415926535
#macro GRAVITY 0.8
#macro RING_TIMEOUT 30

enum RING {
	NONE,
	BELOW,
	ABOVE
}

#macro STAR_VALUE 100
#macro RING_MULTIPLIER 1000