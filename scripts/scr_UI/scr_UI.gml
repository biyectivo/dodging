
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		//if live_call() return live_result;
		var _w = window_get_width();
		var _h = window_get_height();
		
		var _y_title = 120;
		
		type_formatted(_w/2, _y_title, "[scale,1.5][spr_Star][scale,1][fnt_Title][fa_middle][fa_center][c_white]One     room[scale,1.5][spr_Star][scale,1]");
		
		var _channel = animcurve_get_channel(CSSTransitions, "ease-out-bounce");	
		
		if (alarm[3] != -1) {
			var _y = animcurve_channel_evaluate(_channel, 1-alarm[3]/90);			
			var _y_b = -64 + _y*(_y_title+64);			
		}
		else {
			var _y_b = _y_title;
		}
		
		type_formatted(_w/2, _y_b, "[fnt_Title][fa_middle][fa_center][c_gold]b");
		
		type_formatted(_w/2, _y_title+60, "[scale,0.8][fnt_Text][fa_middle][fa_center][c_white]by José Alberto Bonilla Vera (biyectivo) for [c_gold]ScoreSpace Jam 13[c_white] (2021)");		
		
		
		fnc_Link(_w-160, _h-85, "[fa_middle][fa_center][c_white][fnt_Menu]Start", "[spr_Star] [fa_middle][fa_center][c_gold][fnt_Menu]Start [spr_Star]", fnc_ExecuteMenu, 0);
		
		// Icons
		var _icon_help = ICON_HELP;
		fnc_Link(360, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_help, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_help, fnc_Help, 0, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]How to play", 10, 20);
		var _icon_music = Game.option_value[? "Music"] ? ICON_MUSIC_ON : ICON_MUSIC_OFF;
		fnc_Link(300, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_music, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_music, fnc_Music, 0, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]Music on/off", 10, 20);
		var _icon_sound = Game.option_value[? "Sounds"] ? ICON_SOUND_ON : ICON_SOUND_OFF;
		fnc_Link(240, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_sound, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_sound, fnc_Sounds, 1, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]Sounds on/off", 15, 20);		
		var _icon_controls = ICON_CONTROLS;
		fnc_Link(180, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_controls, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_controls, fnc_Controls, 0, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]Controls", 0, 20);
		var _icon_sliders = ICON_SLIDERS;
		fnc_Link(120, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_sliders, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_sliders, fnc_Sliders, 0, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]Sensitivity", 0, 20);
		var _icon_fullscreen = Game.option_value[? "Fullscreen"] ? ICON_FULLSCREEN_ON : ICON_FULLSCREEN_OFF;
		fnc_Link(60, _h-60, "[scale,0.4][fa_bottom][fa_left][c_white][fnt_Icons]"+_icon_fullscreen, "[scale,0.4][fa_bottom][fa_left][c_gold][fnt_Icons]"+_icon_fullscreen, fnc_ExecuteOptions, 3, "[fnt_Text][fa_bottom][fa_left][c_white][scale, 0.5]Fullscreen on/off", 15, 20);
		
		
		
		// Witch name			
		if (name_being_modified) {
			fnc_Link(_w/5, _h/2, "[fa_middle][fa_center][c_gold][fnt_Menu]Type Witch Name: ", "[fa_middle][fa_center][c_gold][fnt_Menu]Type Witch Name: ", noone, 0);
			fnc_Link(_w/5, _h/2+40, "[fa_middle][fa_center][c_white][fnt_Menu]"+option_value[? "Name"]+"[c_gold]_", "[fa_middle][fa_center][c_white][fnt_Menu]"+option_value[? "Name"]+"[c_gold]_", noone, 0);
			if (keyboard_lastkey == vk_enter) { // finalize
				option_value[? "Name"] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
				name_being_modified = false;				
			}
			else {
				keyboard_string = string_copy(keyboard_string,1,16);
				option_value[? "Name"] = keyboard_string;				
			}
		}
		else {			
			fnc_Link(_w/5, _h/2, "[fa_middle][fa_center][c_gold][fnt_Menu]Witch Name", "[fa_middle][fa_center][c_gold][fnt_Menu]Witch Name", noone, 0);
			fnc_Link(_w/5, _h/2+40, "[fa_middle][fa_center][c_white][fnt_Menu]"+option_value[? "Name"], "[fa_middle][fa_center][c_gold][fnt_Menu]"+option_value[? "Name"], fnc_Name, 0);
		}
		
		
		// Witch color controls
		var _link_color = (color_being_modified == 0) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2-90, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Hat color", "[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Hat color", fnc_ActivateChangeColor, 0);
		var _link_color = (color_being_modified == 1) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2-60, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Hair color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Hair color", fnc_ActivateChangeColor, 1);
		var _link_color = (color_being_modified == 2) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2-30, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Skin color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Skin color", fnc_ActivateChangeColor, 2);
		var _link_color = (color_being_modified == 3) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Eye color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Eye color", fnc_ActivateChangeColor, 3);
		var _link_color = (color_being_modified == 4) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2+30, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Shirt color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Shirt color", fnc_ActivateChangeColor, 4);
		var _link_color = (color_being_modified == 5) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2+60, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Sleeve color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Sleeve color", fnc_ActivateChangeColor, 5);
		var _link_color = (color_being_modified == 6) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2+90, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Pants color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Pants color", fnc_ActivateChangeColor, 6);
		var _link_color = (color_being_modified == 7) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2+120, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Shoe color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Shoe color", fnc_ActivateChangeColor, 7);
		var _link_color = (color_being_modified == 8) ? "c_gold" : "c_gray";
		fnc_Link(_w*4/5-90, _h/2+150, "[fa_middle][fa_right]["+_link_color+"][fnt_Menu][scale,0.5]Broom color","[fa_middle][fa_right][c_gold][fnt_Menu][scale,0.5]Broom color", fnc_ActivateChangeColor, 8);
		
		// Color picker
		if (color_being_modified != -1) {		
			if (mouse_wheel_up()) current_color_value = min(current_color_value+16, 255);
			if (mouse_wheel_down()) current_color_value = max(current_color_value-16, 0);
		
			for (var _hue = 0; _hue < 256; _hue = _hue+16) {
				for (var _sat = 0; _sat < 256; _sat = _sat + 16) {
					var _color = make_color_hsv(_hue, _sat, current_color_value);
					var _x1 = _w*4/5-60+_hue;
					var _y1 = _h/2-100+_sat;
					var _x2 = _w*4/5-60+_hue+14;
					var _y2 = _h/2-100+_sat+14;
					var _mousex = device_mouse_x_to_gui(0);
					var _mousey = device_mouse_y_to_gui(0);
					draw_rectangle_color(_x1, _y1, _x2, _y2, _color, _color, _color, _color, false);
					if (_mousex > _x1 && _mousex < _x2 && _mousey > _y1 && _mousey < _y2) {
						draw_rectangle_color(_x1, _y1, _x2, _y2, c_white, c_white, c_white, c_white, true);
					}
					if (device_mouse_check_button_pressed(0, mb_left) && _mousex > _x1 && _mousex < _x2 && _mousey > _y1 && _mousey < _y2) {
						fnc_ChangeColor(_hue, _sat);
						color_click_x = -1;
						color_click_y = -1;
					}					
					/*
					else if (device_mouse_check_button_pressed(0, mb_left) && (device_mouse_x_to_gui(0) != color_click_x || device_mouse_y_to_gui(0) != color_click_y)) {
						color_being_modified = -1;
						color_click_x = -1;
						color_click_y = -1;
					}*/
				}
			}
			type_formatted(_w*4/5-60, _h/2-115, "[fa_left][fa_middle][fnt_Text][scale,0.5][c_white]Change luminosity with mouse wheel!");
		}
		
		// To test the palette swapper in HTML5
		//draw_sprite_ext(sprite_palette, 0, 60, 60, 10, 10, 0, c_white, 1);
		
		// Witch drawing, using palette swapper
		//pal_swap_set(Game.sprite_palette,1,false);
		pal_swap_set(Game.palette_surface, 1, true);
		draw_sprite_ext(spr_Player_Fly, 0, _w/2, _h/2+40, 8, 8, 0, c_white, 1);
		draw_sprite_ext(spr_Broom, 0, _w/2, _h/2+40, 8, 8, 0, c_white, 1);
		pal_swap_reset();
		
		// Draw controls
		if (draw_controls) {			
			draw_set_alpha(0.5);
			draw_roundrect_color_ext(_w/2-100, _h-120, _w/2+300, _h-20, 15, 15, c_black, c_black, false);
			draw_set_alpha(1);
			
			if (wait_for_input) {
				type_formatted(_w/2-80, _h-85, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]Press key");
				type_formatted(_w/2-80, _h-65, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]to remap");
				type_formatted(_w/2-80, _h-45, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]ESC to cancel");
			}
			else {
				type_formatted(_w/2-80, _h-75, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]Controls");
				type_formatted(_w/2-82, _h-55, "[fnt_Text][scale,0.5][c_white][fa_left][fa_middle]Click to Remap");				
			}
			
			
			var _n = ds_map_size(controls);
			for (var _i=0; _i<_n; _i++) {				
				if (wait_for_input) {					
					if (key_being_remapped == _i) {					
						type_formatted(_w/2+80, _h-100+_i*20, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_gold]"+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]));						
					}
					else {
						type_formatted(_w/2+80, _h-100+_i*20, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]"+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]));
					}					
				}
				else {
					fnc_Link(_w/2+80, _h-100+_i*20, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]"+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_Text][c_gold][scale,0.5]"+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, _i);
				}
			}
			if (wait_for_input) {
				if (keyboard_lastkey != -1 && keyboard_lastkey != vk_escape) {
					controls[? control_indices[key_being_remapped]] = keyboard_lastkey;
					wait_for_input = false;
					key_being_remapped = -1;
				}
				else if (keyboard_lastkey == vk_escape) {
					wait_for_input = false;
					key_being_remapped = -1;
				}
			}
		}
		
		
		// Draw help text
		if (draw_help_text) {			
			draw_set_alpha(0.5);
			draw_roundrect_color_ext(_w/2-100, _h-120, _w/2+300, _h-20, 15, 15, c_black, c_black, false);
			draw_set_alpha(1);
			
			type_formatted(_w/2-80, _h-75, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]How to");
			type_formatted(_w/2-80, _h-55, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle]Play");
						
			type_formatted(_w/2+5, _h-100, "[fa_middle][fa_left][fnt_Text][scale,0.5]Follow the [c_gold]arrow[c_white] [spr_Arrow,1] to fly through the [c_gold]hoops[c_white][spr_Ring]");
			type_formatted(_w/2+5, _h-80, "[fa_middle][fa_left][fnt_Text][scale,0.5]Collect [c_gold]falling stars[c_white] [spr_Star]");			
			type_formatted(_w/2+5, _h-60, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_gold]Dodge[c_white] the angry Gods'[c_gold] lightning! [spr_Spell,0,20]");				
			type_formatted(_w/2+5, _h-40, "[fa_middle][fa_left][fnt_Text][scale,0.5]Points:[spr_Ring] [c_gold]"+string(RING_MULTIPLIER)+"[c_white] multiplier! [spr_Star] [c_gold]"+string(STAR_VALUE)+"[c_white] each");
			
		}
		
		// Draw sliders
		if (draw_sliders) {			
			draw_set_alpha(0.5);
			draw_roundrect_color_ext(_w/2-100, _h-120, _w/2+300, _h-20, 15, 15, c_black, c_black, false);
			draw_set_alpha(1);
			
			type_formatted(_w/2-80, _h-65, "[fnt_Menu][scale,0.5][c_white][fa_left][fa_middle][c_white]Sensitivity");			
			
			type_formatted(_w/2+60, _h-100, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]Acceleration: ");
			type_formatted(_w/2+180, _h-100, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]"+Game.sensitivity_text[Game.acceleration_sensitivity_index]);
			fnc_Link(_w/2+260, _h-110, "[c_white][fnt_Menu][scale,0.7]>", "[c_gold][fnt_Menu][scale,0.7]>", fnc_SetAcceleration, 0);
			type_formatted(_w/2+60, _h-70, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]Deceleration: ");
			type_formatted(_w/2+180, _h-70, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]"+Game.sensitivity_text[Game.deceleration_sensitivity_index]);
			fnc_Link(_w/2+260, _h-80, "[c_white][fnt_Menu][scale,0.7]>", "[c_gold][fnt_Menu][scale,0.7]>", fnc_SetDeceleration, 0);
			type_formatted(_w/2+60, _h-40, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]Steering speed: ");
			type_formatted(_w/2+180, _h-40, "[fa_middle][fa_left][fnt_Text][scale,0.5][c_white]"+Game.sensitivity_text[Game.steering_sensitivity_index]);
			fnc_Link(_w/2+260, _h-50, "[c_white][fnt_Menu][scale,0.7]>", "[c_gold][fnt_Menu][scale,0.7]>", fnc_SetSteeringSpeed, 0);
			
		}
	}
	
	function fnc_SetSteeringSpeed() {
		Game.steering_sensitivity_index = (Game.steering_sensitivity_index+1) % array_length(Game.steering_sensitivity);
	}
	function fnc_SetAcceleration() {
		Game.acceleration_sensitivity_index = (Game.acceleration_sensitivity_index+1) % array_length(Game.acceleration_sensitivity);
	}
	function fnc_SetDeceleration() {
		Game.deceleration_sensitivity_index = (Game.deceleration_sensitivity_index+1) % array_length(Game.deceleration_sensitivity);
	}
	
	
	function fnc_Music() {
		Game.option_value[? "Music"] = !Game.option_value[? "Music"];
		if (audio_is_playing(snd_Intro) && !Game.option_value[? "Music"]) {
			audio_pause_sound(snd_Intro);
		}		
		else if (Game.option_value[? "Music"]) {
			if (audio_is_paused(snd_Intro)) {
				audio_resume_sound(snd_Intro);	
			}
			else {
				audio_play_sound(snd_Intro, 1, true);	
			}
		}
	}
	
	function fnc_Sounds() {
		Game.option_value[? "Sounds"] = !Game.option_value[? "Sounds"];
	}
	
	function fnc_ActivateChangeColor() {
		if (color_being_modified == -1) {
			color_being_modified = argument[0];
			color_click_x = device_mouse_x_to_gui(0);
			color_click_y = device_mouse_y_to_gui(0);
		}
	}
	
	function fnc_ChangeColor(_hue, _sat) {		
		/*surface_set_target(Game.palette_surface);
		draw_sprite(sprite_palette, 0, 0, 0);
		draw_set_color(make_color_hsv(_hue, _sat, current_color_value));
		draw_point(1, color_being_modified);		
		surface_reset_target();
		color_being_modified = -1;	*/
		colors[color_being_modified] = make_color_hsv(_hue, _sat, current_color_value);		
		surface_set_target(Game.palette_surface);
		draw_set_color(colors[color_being_modified]);
		draw_point(1, color_being_modified);
		surface_reset_target();
		color_being_modified = -1;
		//surface_free(palette_surface); //force refresh
	}

	function fnc_Controls() {
		draw_controls = !draw_controls;
		if (draw_controls) {
			draw_help_text = false;
			draw_sliders = false;
		}
	}
	
	function fnc_Help() {
		draw_help_text = !draw_help_text;
		if (draw_help_text) {
			draw_controls = false;
			draw_sliders = false;
		}
	}
	
	function fnc_Name() {
		if (!name_being_modified) {			
			keyboard_lastkey = -1;
			name_being_modified = true;
		}
	}
	
	function fnc_Sliders() {
		draw_sliders = !draw_sliders;
		if (draw_sliders) {
			draw_controls = false;
			draw_help_text = false;
		}
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	
	
	function fnc_AssignControls(_key) {	
		wait_for_input = true;
		keyboard_lastkey = -1;
		key_being_remapped = _key;
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	//if live_call() return live_result;
	var _w = window_get_width();
	var _h = window_get_height();
	
	draw_set_alpha(0.6);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	
	if (!already_played_death_sound) {
		if (Game.option_value[? "Music"]) {
			audio_play_sound(snd_Death, 1, false);
		}
		already_played_death_sound = true;
	}
	
	// Witch drawing, using palette swapper
	//pal_swap_set(Game.sprite_palette,1,false);
	pal_swap_set(Game.palette_surface, 1, true);
	draw_sprite_ext(spr_Player_Fly, 0, _w/2, 40, 2, 2, 0, c_white, 1);
	draw_sprite_ext(spr_Broom, 0, _w/2, 40, 2, 2, 0, c_white, 1);
	pal_swap_reset();
	
	type_formatted(_w/2, 100, "[fa_middle][fa_center][fnt_Menu][c_white][scale,0.8]The witch known as [c_gold]"+Game.option_value[? "Name"]+"[c_white] died :(");
	
	type_formatted(_w/2, 140, "[fa_middle][fa_center][fnt_Menu][spr_Star] Score: "+string(Game.total_score)+" [spr_Star]");
	type_formatted(_w/2, 170, "[fa_middle][fa_center][fnt_Text][scale,0.7][c_white]Passed through [c_gold]"+string(Game.total_hoops)+"[c_white] hoops and collected [c_gold]"+string(Game.total_stars)+"[c_white] stars.");
	
	if (ENABLE_SCOREBOARD) {
		// Scoreboard
		if (!scoreboard_queried) {
			//http_call = "query_scoreboard";
			var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
			http_get_id_query = http_get(_scoreboard_url);
			scoreboard_queried = true;
			if (current_scoreboard_updates < max_scoreboard_updates) {				
				Game.alarm[4] = timer_scoreboard_updates;
				current_scoreboard_updates++;
			}
		}
		else {
			if (http_return_status_query == 200) {
				
							
				var _list = ds_map_find_value(scoreboard_html5, "default");
				var _n = ds_list_size(_list);
				for (var _i=0; _i<_n; _i++) {
					var _map = ds_list_find_value(_list, _i);
					draw_set_color(c_white);
						
					if (_i==0) {
						var _total_high_score = _map[? "game_score"];
					}
					else if (_i==4) {
						var _number_5_score	= _map[? "game_score"]; 
					}
					
					if (Game.total_score == _map[? "game_score"] && Game.option_value[? "Name"] == _map[? "username"]) {
						var _color = "c_aqua";
					}
					else {
						var _color = "c_white";
					}
					
					type_formatted(_w/4-80, _h*2/5+_i*50, "[fa_left][fa_middle][fnt_Menu][c_gold]#"+string(_i+1));
					type_formatted(_w/4, _h*2/5+_i*50, "[fa_left][fa_middle][fnt_Menu]["+_color+"]"+_map[? "username"]);
					type_formatted(_w*3/4+100, _h*2/5+_i*50, "[fa_right][fa_middle][fnt_Menu]["+_color+"]"+string(_map[? "game_score"]));
				}
				
				if (Game.total_score >= _total_high_score) {
					type_formatted(_w/2, 220, "[fa_center][fa_middle][fnt_Menu][c_gold][scale,0.5][spr_Star][scale,0.6][spr_Star][scale,0.7][spr_Star][scale,0.8] You just set a new World Record!!! [scale,0.7][spr_Star][scale,0.6][spr_Star][scale,0.5][spr_Star]");
				}
				else if (Game.total_score >= _number_5_score) {
					type_formatted(_w/2, 220, "[fa_center][fa_middle][fnt_Menu][c_aqua][scale,0.5][spr_Star][scale,0.8] You reached the top 5! [scale,0.5][spr_Star]");
				}
				
				
			}
			else if (http_return_status_query == noone) {
				type_formatted(_w/2, 400, "[fa_center][fa_middle][fnt_Menu]Loading scoreboard...");
			}			
		}
	}
	
	fnc_Link(_w/2, _h-120, "[fa_center][fa_middle][fnt_Menu][c_white]Restart", "[fa_center][fa_middle][fnt_Menu][c_gold]Restart", fnc_TryAgain, 0);
	fnc_Link(_w/2, _h-60,  "[fa_center][fa_middle][fnt_Menu][c_white]Return to Menu", "[fa_center][fa_middle][fnt_Menu][c_gold]Return to Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawHUD() {
	fnc_BackupDrawParams();
	// Draw the HUD
	var _w = window_get_width();
	var _h = window_get_height();
	
	//draw_healthbar(10, 10, 100, 30, floor(100*obj_Player.fly_speed/obj_Player.max_fly_speed), false, c_aqua, fnc_TypeFormatted_Color("c_gold"), 0, true, true);
	type_formatted(_w-20, 20, "[fa_right][fa_top][fnt_Menu][c_white]Score: "+string(Game.total_score));
	/*draw_set_font(fnt_Menu);
	draw_set_halign(fa_right);
	draw_set_valign(fa_top);
	draw_set_color(c_white);
	draw_text(_w-20, 20, "Score: "+string(Game.total_score));*/
	type_formatted(_w-20, 80, "[fa_right][fa_top][fnt_Menu][c_white][scale,0.6]Hoops crossed: "+string(Game.total_hoops));
	type_formatted(_w-20, 120, "[fa_right][fa_top][fnt_Menu][c_white][scale,0.6]Stars collected: "+string(Game.total_stars));
	
	
	fnc_RestoreDrawParams();
}

#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.7);
		//draw_rectangle(0, 0, adjusted_window_width, adjusted_window_height, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(10, 10,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(10, 25, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.state);
		}
		else {
			draw_text(10, 25, "Player not in room");	
		}
		var _spacing = 30;
		
		var _debug_data = [];
		
		_debug_data[0] = "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")";
		_debug_data[1] = "Requested scaling type: "+string(SELECTED_SCALING)+" ("+(SELECTED_SCALING==SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION ? "Window determined by resolution" : (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION ? "Window independent of resolution" : "Resolution scaled to window"));
		_debug_data[2] = "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H);
		_debug_data[3] = "Actual base resolution: "+string(adjusted_camera_width)+"x"+string(adjusted_camera_height);
		_debug_data[4] = "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H);
		_debug_data[5] = "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height);
		_debug_data[6] = "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")";
		_debug_data[7] = "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")";
		_debug_data[8] = "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")";
		_debug_data[9] = "GUI Layer: "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")";
		_debug_data[10] = "Mouse: "+string(mouse_x)+","+string(mouse_y);
		_debug_data[11] = "Device mouse 0: "+string(device_mouse_x(0))+","+string(device_mouse_y(0));
		_debug_data[12] = "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0));		
		_debug_data[13] = "Paused: "+string(paused);
		_debug_data[14] = "FPS: "+string(fps_real) + "/" + string(fps);
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(10, 40+_i*15, _debug_data[_i]);
			//show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param /*, _tooltip, _tooltip_x_offset, _tooltip_y_offset*/) {
	
	if (argument_count > 6) {
		var _tooltip = argument[6];	
	}
	else {
		var _tooltip = "";	
	}
	if (argument_count > 7) {
		var _tooltip_x_offset = argument[7];	
	}
	else {
		var _tooltip_x_offset  = 0;
	}
	if (argument_count > 8) {
		var _tooltip_y_offset  = argument[8];	
	}
	else {
		var _tooltip_y_offset  = 0;
	}
	
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _mousex = device_mouse_x_to_gui(0);
	var _mousey = device_mouse_y_to_gui(0);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = _mousex >= _bbox_coords[0] && _mousey >= _bbox_coords[1] && _mousex <= _bbox_coords[2] && _mousey <= _bbox_coords[3];
	//var _mouseover = _mousex >= _draw_data_normal.bbox_x1 && _mousey >= _draw_data_normal.bbox_y1 && _mousex <= _draw_data_normal.bbox_x2 && _mousey <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			script_execute(_callback, _param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_tooltip != "") {
			type_formatted(_x+_tooltip_x_offset, _y+_tooltip_y_offset, _tooltip);
		}
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	script_execute(asset_get_index("fnc_Menu_"+string(_param)));
}

function fnc_ExecuteOptions(_param) {
	script_execute(asset_get_index("fnc_Options_"+string(_param)));
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion